//
//  ACToolBarView.swift
//  AskClara
//
//  Created by Donovan King on 4/12/17.
//  Copyright © 2017 Donovan King. All rights reserved.
//

import Foundation
import UIKit
import JSQMessagesViewController

class ACToolBarView: UIView, LeanTaaSThreadingMixin {
    
    @IBOutlet var view: UIView!
    @IBOutlet weak var tableView: UITableView!
        
    private var menuOpen = false {
        didSet {
            menuOpen ? darken() : unDarken()
        }
    }
    
    var isMenuOpen: Bool {
        get {
            return menuOpen
        }
    }
    
    override init(frame: CGRect) {
        if (frame.width == 0 && frame.height == 0) {
            super.init(frame: CGRect(x: 0,
                                     y: UIScreen.main.bounds.height - 45,
                                     width: UIScreen.main.bounds.width,
                                     height: 250 + 50))
        } else {
            super.init(frame: frame)
        }
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    @IBAction func showHideMenu(_ sender: Any) {
        menuOpen ? hideMenu() : showMenu()
    }
    
    private func setupView() {
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        
        // Show the view.
        addSubview(view)
        
        
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "basic")
        tableView.tableFooterView = UIView()
        view.bringSubview(toFront: tableView)
    }
    
    private func viewFromNibForClass() -> UIView {
         let bundle = Bundle(for: type(of: self))
         let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
         let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
         return view
    }
    
    func showMenu() {

        let newFrame = CGRect(x: 0,
                                 y: self.frame.origin.y - 205,
                                 width: self.frame.width,
                                 height: self.frame.height)
        
        UIView.animate(withDuration: 0.6,
                       delay: 0,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 10,
                       options: .curveEaseIn,
                       animations: { self.frame = newFrame })
        menuOpen = true
    }
    
    func hideMenu() {
        let newFrame = CGRect(x: 0,
                                 y: self.frame.origin.y + 205,
                                 width: self.frame.width,
                                 height: self.frame.height)
        
        
        UIView.animate(withDuration: 0.6,
                       delay: 0,
                       usingSpringWithDamping: 0.9,
                       initialSpringVelocity: 10,
                       options: .curveEaseIn,
                       animations: { self.frame = newFrame })
        menuOpen = false
    }
    
    var darkView: UIView!
    
    func darken() {
        darkView = UIView(frame: CGRect(x: 0,
                                            y: -UIScreen.main.bounds.height,
                                            width: UIScreen.main.bounds.width,
                                            height: UIScreen.main.bounds.height))
        darkView.backgroundColor = UIColor.black
        darkView.alpha = 0
        view.addSubview(darkView)
        view.sendSubview(toBack: darkView)
        UIView.animate(withDuration: 0.35) {
            self.darkView.alpha = 0.5
        }
    }
    
    func unDarken() {
        if self.darkView == nil { return }
        UIView.animate(withDuration: 0.4, animations: {
            self.darkView.alpha = 0
        }) { (success) in
            self.darkView.removeFromSuperview()
        }
    }
    
}

