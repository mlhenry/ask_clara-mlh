//
//  MessagingVC+Actions.swift
//  AskClara
//
//  Created by Donovan King on 5/2/17.
//  Copyright © 2017 Donovan King. All rights reserved.
//

import Foundation
import UIKit
import JSQMessagesViewController

// Custom Action Functionality



extension MessagingViewController {
    
    override func collectionView(_ collectionView: UICollectionView,
                                 canPerformAction action: Selector,
                                 forItemAt indexPath: IndexPath,
                                 withSender sender: Any?) -> Bool {
        return demoControl.getMessage(Item: indexPath.item).isMediaMessage()
    }
    
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return demoControl.getMessage(Item: indexPath.item).isMediaMessage()
    }
    
    override func collectionView(_ collectionView: UICollectionView,
                                 performAction action: Selector,
                                 forItemAt indexPath: IndexPath,
                                 withSender sender: Any?) {
        
        if action == #selector(JSQMessagesCollectionViewCell.share) {
            self.startShareScript()
        }
    }
    
    private func startShareScript() {
        replyView.hide()
        demoControl.beginShareFlow()
    }
    
    
}
