//
//  PinEntryView.swift
//  AskClara
//
//  Created by Donovan King on 4/11/17.
//  Copyright © 2017 Donovan King. All rights reserved.
//

import Foundation
import UIKit

class PinEntryView: UIView, UIKeyInput {
    
    
    var hasText: Bool = false
    var keyboardType: UIKeyboardType = UIKeyboardType.numberPad
    private let empty = #imageLiteral(resourceName: "dot-empty")
    private let filled = #imageLiteral(resourceName: "dot-filled")
    private var currentDiget = 0 {
        didSet {
            self.updateDotState()
        }
    }
    
    weak var pinViewDelegate: ACPinViewDelegate?
    
    @IBOutlet weak var dot1: UIImageView!
    @IBOutlet weak var dot2: UIImageView!
    @IBOutlet weak var dot3: UIImageView!
    @IBOutlet weak var dot4: UIImageView!
    
    

    override var canBecomeFirstResponder: Bool{
        get {
            return true
        }
    }
    
    func insertText(_ text: String) {
        
        currentDiget = currentDiget + 1
    }
    
    func deleteBackward() {
        currentDiget = currentDiget - 1
    }
    
    private func updateDotState() {
        switch currentDiget {
        case 1:
            dot1.image = filled
            dot2.image = empty
            dot3.image = empty
            dot4.image = empty
        case 2:
            dot1.image = filled
            dot2.image = filled
            dot3.image = empty
            dot4.image = empty
        case 3:
            dot1.image = filled
            dot2.image = filled
            dot3.image = filled
            dot4.image = empty
        case 4:
            dot1.image = filled
            dot2.image = filled
            dot3.image = filled
            dot4.image = filled
            pinViewDelegate?.pinComplete(EnteredPin: "")
        default:
            dot1.image = empty
            dot2.image = empty
            dot3.image = empty
            dot4.image = empty
        }
    }
    
}

protocol ACPinViewDelegate: class {
    func pinComplete(EnteredPin: String)
}
