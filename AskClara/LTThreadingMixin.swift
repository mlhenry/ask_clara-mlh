//
//  LTThreadingMixin.swift
//  ZSP
//
//  Created by Donovan King on 9/1/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation


protocol LeanTaaSThreadingMixin {
    
}

extension LeanTaaSThreadingMixin {
    var GlobalMainQueue: DispatchQueue {
        return DispatchQueue.main
    }
    
    var GlobalUserInteractiveQueue: DispatchQueue {
        return DispatchQueue.global(qos: DispatchQoS.QoSClass.userInteractive)
    }
    
    var GlobalUserInitiatedQueue: DispatchQueue {
        return DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated)
    }
    
    var GlobalUtilityQueue: DispatchQueue {
        return DispatchQueue.global(qos: DispatchQoS.QoSClass.utility)
    }
    
    var GlobalBackgroundQueue: DispatchQueue {
        return DispatchQueue.global(qos: DispatchQoS.QoSClass.background)
    }
}
