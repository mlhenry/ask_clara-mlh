//
//  ACCellIncoming.swift
//  AskClara
//
//  Created by Donovan King on 4/25/17.
//  Copyright © 2017 Donovan King. All rights reserved.
//

import Foundation
import UIKit
import JSQMessagesViewController
import SwiftyGif

extension JSQMessagesCollectionViewCell {
    
    open override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return action == #selector(share)
    }
    
    func share(sender: Any) {
        delegate.messagesCollectionViewCell(self,
                                            didPerformAction: #selector(share),
                                            withSender: self)
    }
    
    

}

class ACMessage: JSQMessage {
    
    
    init(senderId: String!, senderDisplayName: String!, date: Date!, text: String!, gif: UIImage) {
        let gifMediaItem = ACMediaItem(gif: gif, text: text)
        super.init(senderId: senderId,
                   senderDisplayName: senderDisplayName,
                   date: date,
                   media: gifMediaItem)
    }
    
    init(senderId: String!, senderDisplayName: String!, date: Date!, text: String!, chart: UIImage) {
        let chartMediaItem = ACMediaItem(image: chart, text: text)
        super.init(senderId: senderId,
                   senderDisplayName: senderDisplayName,
                   date: date,
                   media: chartMediaItem)
    }
    
    override init!(senderId: String!, senderDisplayName: String!, date: Date!, text: String!) {
        super.init(senderId: senderId,
                   senderDisplayName: senderDisplayName,
                   date: date,
                   text: text)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

class ACMediaItem: JSQMediaItem {
    
    var image: UIImage
    var text: String
    var gifManager: SwiftyGifManager?
    var view: UIView?
    
    init(gif: UIImage, text: String) {
        self.image = gif
        self.text = text
        self.gifManager = SwiftyGifManager(memoryLimit: 50)
        super.init(maskAsOutgoing: false)
    }
    
    init(image: UIImage, text: String) {
        self.image = image
        self.text = text
        super.init(maskAsOutgoing: false)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("coder error on ACMediaItem class")
    }

    override func mediaView() -> UIView! {
        
        if let mediaView = self.view {
            return mediaView
        }
        
        let contentWidth: CGFloat
        
        if (UIScreen.main.bounds.width - 30) >= (image.size.width + 30) && gifManager == nil {
            contentWidth = image.size.width + 30
        } else {
            contentWidth = UIScreen.main.bounds.width - 30
        }

        
        let contentView = WidgetCellView(frame: CGRect(x: 6, y: 0, width: contentWidth, height: getContentHeight()))
        
        if let manager = gifManager {
            contentView.imageView.setGifImage(image, manager: manager)
            if !contentView.imageView.isAnimatingGif() {
                contentView.imageView.startAnimatingGif()
            }
            contentView.imageView.contentMode = .scaleAspectFit
        } else {
            contentView.imageView.image = image
        }
        
        contentView.textLabel.text = text
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: contentWidth + 6, height: contentView.frame.height))
        
        let bubbleImage = JSQMessagesBubbleImageFactory().incomingMessagesBubbleImage(with: UIColor.white)
        let bubble = UIImageView(frame: view.frame)
        bubble.image = bubbleImage?.messageBubbleImage
        view.addSubview(bubble)
        view.addSubview(contentView)
        self.view = view
        return view
    }
    
    override func mediaViewDisplaySize() -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width * 0.8, height: getContentHeight())
    }
    
    override func mediaHash() -> UInt {
        return super.mediaHash() ^ UInt(image.hash)
    }
    
    private func getContentHeight() -> CGFloat {
        let padding: CGFloat = 31
        let textHeight: CGFloat = 25
        var imageHeight = image.size.height
        if imageHeight == 0 {
            imageHeight = 200
        }
        return padding + textHeight + imageHeight
    }
    
    // TODO: Dynamic label sizing---------
    private func rectForText(text: String, font: UIFont, maxSize: CGSize) -> CGSize {
        let attrString = NSAttributedString.init(string: text, attributes: [NSFontAttributeName:font])
        let rect = attrString.boundingRect(with: maxSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
        return CGSize(width: rect.size.width, height: rect.size.height)
    }
    
}


class WidgetCellView: UIView {
    
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        let view = viewFromNibForClass()
        view.frame = bounds
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        addSubview(view)
    }
    
    private func viewFromNibForClass() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
}

