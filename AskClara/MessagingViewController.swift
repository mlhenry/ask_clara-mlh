//
//  MessagingViewController.swift
//  AskClara
//
//  Created by Donovan King on 4/11/17.
//  Copyright © 2017 Donovan King. All rights reserved.
//

import Foundation
import UIKit
import JSQMessagesViewController

class MessagingViewController: JSQMessagesViewController,
                               ReplyViewDelegate,
                               ACFlowControlDelegate,
                               LeanTaaSThreadingMixin {
    
    var demoControl = ACDemoFlowController()
    var replyView: ACReplyView!
    var toolBar: ACToolBarView!
    
    lazy var outgoingBubbleImageView: JSQMessagesBubbleImage = self.setupOutgoingBubble()
    lazy var incomingBubbleImageView: JSQMessagesBubbleImage = self.setupIncomingBubble()
    

    
    var savedOffset = CGPoint()
    var savedInset = UIEdgeInsets()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        JSQMessagesCollectionViewCell.registerMenuAction(#selector(JSQMessagesCollectionViewCell.share))
        let share = UIMenuItem(title: "Share", action: #selector(JSQMessagesCollectionViewCell.share))
        UIMenuController.shared.menuItems = [share]
        
        demoControl.delegate = self
        
        automaticallyScrollsToMostRecentMessage = true
        automaticallyAdjustsScrollViewInsets = false
        
        collectionView.contentInset = UIEdgeInsets(top: 63, left: 0, bottom: 150, right: 0)
        
        
        collectionView.dataSource = self
        collectionView.typingIndicatorMessageBubbleColor = UIColor.white
        
        senderId = demoControl.user.id
        senderDisplayName = demoControl.user.name
        
        collectionView.collectionViewLayout.incomingAvatarViewSize = .zero
        collectionView.collectionViewLayout.outgoingAvatarViewSize = .zero
        
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.layer.bounds
        let color1 = UIColor(red:0.94, green:1.00, blue:0.98, alpha:1.0).cgColor
        let color2 = UIColor(red:0.67, green:0.99, blue:0.88, alpha:1.0).cgColor
        gradientLayer.colors = [color1, color2]
        
        gradientLayer.locations = [0.0, 1.0]
        self.view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        demoControl.begin()
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return action == #selector(JSQMessagesCollectionViewCell.share)
    }
    
    @available(iOS 6.0, *)
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return demoControl.getMessageCount() // + userReplyOptions.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        
        return demoControl.getMessage(Item: indexPath.item)
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didDeleteMessageAt indexPath: IndexPath!) {
        
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = demoControl.getMessage(Item: indexPath.item)
        if message.senderId() == senderId {
            return outgoingBubbleImageView
        }
        return incomingBubbleImageView
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        return nil
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell

        let message = demoControl.getMessage(Item: indexPath.item)
        if message.senderId() == senderId {
            cell.textView?.textColor = UIColor.white
        } else {
            cell.textView?.textColor = UIColor.black
        }
        
        if !demoControl.getMessage(Item: indexPath.item).isMediaMessage() {
            cell.textView.isUserInteractionEnabled = false
        }
        
        return cell
    }
    
    func replyViewWillReveal() {
        savedInset = collectionView.contentInset
        
        self.collectionView.contentInset = UIEdgeInsetsMake(self.savedInset.top,
                                                            self.savedInset.left,
                                                            replyView.frame.height + 45 + 25,
                                                            self.savedInset.right)
        scrollToBottom(animated: true)
    }
    
    func myScrollToBottom() {
        let y = collectionView.contentSize.height -
                collectionView.bounds.size.height +
                collectionView.contentInset.bottom +
                collectionView.contentInset.top
        let contentOffset = CGPoint(x: 0, y: y)
        UIView.animate(withDuration: 0.2) {
            self.collectionView.contentOffset = contentOffset
        }
    }
    
    func replyViewDidHide() {
        UIView.animate(withDuration: 0.1) {
            self.collectionView.contentInset = self.savedInset
        }
    }
    
    func userSelected(Message node: ACReplyOption) {
        demoControl.add(Reply: node)
        
    }
    
    func update() {
        showTypingIndicator = false
        finishReceivingMessage(animated: true)
    }
    
    func animatePendingMessage(callback: @escaping () -> ()) {
        showTypingIndicator = true
        Timer.scheduledTimer(withTimeInterval: 1.5, repeats: false) { (timer) in
            callback()
        }
    }
    
    func requestReply(FromList replies: [ACReplyOption]) {
        self.replyView.showWith(ReplyOptions: replies)
    }
    
    private func setupView() {
        setupNavItem()
        navigationController?.navigationBar.barTintColor = UIColor(red:1.00,
                                                                   green:1.00,
                                                                   blue:1.00,
                                                                   alpha:0.8)
        self.collectionView.backgroundColor = UIColor.clear
        self.inputToolbar.removeFromSuperview()
        setupReplyView()
    }
    
    private func setupNavItem() {
        
        guard let height = navigationController?.navigationBar.layer.bounds.height else {
            return
        }
        
        guard let width = navigationController?.navigationBar.layer.bounds.width else {
            return
        }
        
        let logoImageView = UIImageView(frame: CGRect(x: 0,
                                                  y: 0,
                                                  width: Int(width*0.3),
                                                  height: Int(height-4)))
        logoImageView.image = #imageLiteral(resourceName: "logo-nav")
        logoImageView.contentMode = .scaleAspectFit
        let barButtonLeft = UIBarButtonItem(customView: logoImageView)
        self.navigationItem.leftBarButtonItem = barButtonLeft
        
        let profileImageView = UIImageView(frame: CGRect(x: 0,
                                                         y: 0,
                                                         width: height-8,
                                                         height: height-8))
        profileImageView.image = #imageLiteral(resourceName: "profile-picture")
        profileImageView.contentMode = .scaleAspectFit
        let barButtonRight = UIBarButtonItem(customView: profileImageView)
        self.navigationItem.rightBarButtonItem = barButtonRight
    }
    
    private func setupToolBarView() {
        self.toolBar = ACToolBarView()
        toolBar.tableView.delegate = self
        toolBar.tableView.dataSource = self
        toolBar.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 5, right: 0)
        self.view.insertSubview(toolBar, aboveSubview: replyView)
    }
    
    private func setupReplyView() {
        self.replyView = ACReplyView()
        self.view.insertSubview(replyView, aboveSubview: collectionView)
        self.replyView.delegate = self
        self.setupToolBarView()
    }
    
    private func setupOutgoingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        let userGrey = UIColor(red:0.56, green:0.56, blue:0.58, alpha:1.0)
        return bubbleImageFactory!.outgoingMessagesBubbleImage(with: userGrey)
    }
    
    private func setupIncomingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.incomingMessagesBubbleImage(with: UIColor.white)
    }
    
}

protocol ReplyViewDelegate: class {
    func replyViewWillReveal()
    func replyViewDidHide()
    func userSelected(Message node: ACReplyOption)
    func myScrollToBottom()
}




