//
//  Messaging+ToolMenu.swift
//  AskClara
//
//  Created by Donovan King on 5/2/17.
//  Copyright © 2017 Donovan King. All rights reserved.
//

import Foundation
import UIKit
import JSQMessagesViewController

// Subject menu functionality

extension MessagingViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "basic", for: indexPath)
        
        let item = DemoFlow.menuItems[indexPath.row]
        
        cell.textLabel?.text = item.displayText
        cell.textLabel?.textAlignment = .right
        cell.textLabel?.font = UIFont(name: "SFUIText-Regular", size: 18.0) ?? UIFont.systemFont(ofSize: 18)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) else { return }
        cell.isSelected = false
        self.toolBar.hideMenu()
        self.replyView.hide()
        scrollToBottom(animated: true)
        demoControl.jumpTo(NodeId: DemoFlow.menuItems[indexPath.row].pointerId)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DemoFlow.menuItems.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.autoresizingMask = [UIViewAutoresizing.flexibleWidth,
                                       UIViewAutoresizing.flexibleHeight]
        headerView.backgroundColor = UIColor(red:0.00, green:0.74, blue:0.49, alpha:1.0)
        
        let label = UILabel()
        let attr = [ NSFontAttributeName : UIFont(name: "SFUIText-Regular", size: 18.0) ?? UIFont.systemFont(ofSize: 18),
                     NSForegroundColorAttributeName : UIColor.white]
        let attrString = NSAttributedString(string: "Tell me about my claims...", attributes: attr)
        label.attributedText =  attrString
        label.numberOfLines = 0
        
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        
        headerView.addSubview(label)
        
        let leading = NSLayoutConstraint(item: label,
                                         attribute: .leading,
                                         relatedBy: .equal,
                                         toItem: headerView,
                                         attribute: .leading,
                                         multiplier: 1,
                                         constant: 20)
        let top = NSLayoutConstraint(item: label,
                                     attribute: .top,
                                     relatedBy: .equal,
                                     toItem: headerView,
                                     attribute: .top,
                                     multiplier: 1,
                                     constant: 0)
        let bottom = NSLayoutConstraint(item: label,
                                        attribute: .bottom,
                                        relatedBy: .equal,
                                        toItem: headerView,
                                        attribute: .bottom,
                                        multiplier: 1,
                                        constant: 0)
        let trailing = NSLayoutConstraint(item: label,
                                          attribute: .trailing,
                                          relatedBy: .equal,
                                          toItem: headerView,
                                          attribute: .trailing,
                                          multiplier: 1,
                                          constant: 0)
        
        headerView.addConstraints([leading, top, trailing, bottom])
        return headerView
    }
    
}
