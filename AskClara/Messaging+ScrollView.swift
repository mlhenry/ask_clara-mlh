//
//  Messaging+ScrollView.swift
//  AskClara
//
//  Created by Donovan King on 5/2/17.
//  Copyright © 2017 Donovan King. All rights reserved.
//

import Foundation
import UIKit

extension MessagingViewController {
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.adjustReplyViewFor(ScrollView: scrollView)
    }
    
    private func adjustReplyViewFor(ScrollView scrollView: UIScrollView) {
        if replyView != nil {
            let contentBottomYOffset = replyView.frame.origin.y - (scrollView.contentSize.height - scrollView.contentOffset.y)
            if contentBottomYOffset < 0 && replyView.visible {
                let replyHeight = replyView.frame.height
                let alphaOffset: CGFloat
                let percentOfView = (-contentBottomYOffset) / (replyHeight/2)
                if percentOfView < 0.5 {
                    alphaOffset = 1 - percentOfView
                } else {
                    alphaOffset = 0.5
                }
                replyView.alpha = alphaOffset
            } else {
                replyView.alpha = 1
            }
        }
    }
    
}
