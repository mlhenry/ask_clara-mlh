//
//  ACDemoFlowController.swift
//  AskClara
//
//  Created by Donovan King on 4/20/17.
//  Copyright © 2017 Donovan King. All rights reserved.
//

import Foundation
import UIKit
import JSQMessagesViewController
import SwiftyGif

class ACDemoFlowController: NSObject {

    let nodeStore = ACDemoFlowStore()
    
    private var currentNodeID: String
    private var shareContextReturnID: String
    
    private var pendingHaltNodeId: String?
    private var haltCycle = false {
        didSet {
            if let id = self.pendingHaltNodeId, oldValue && !haltCycle {
                cycle(id: id)
                pendingHaltNodeId = nil
            }
        }
    }
    
    private var cycleLock = false
    
    private var messages = [JSQMessageData]()
    weak var delegate: ACFlowControlDelegate?
    
    let bot: Bot
    let user: User
    
    
    override init() {
        self.currentNodeID = ""
        self.shareContextReturnID = ""
        self.bot = Bot()
        self.user = User()
        let nodes = DemoFlow.nodes
        nodeStore.load(Nodes: nodes)
    }
    
    func begin() {
        cycle(id: "1")
    }
    
    func jumpTo(NodeId id: String) {
        if cycleLock {
            haltCycle = true
            self.pendingHaltNodeId = id
        } else {
            cycle(id: id)
        }
    }
    
    func beginShareFlow() {
        var backToID = shareContextReturnID.isEmpty ? currentNodeID : shareContextReturnID
        if backToID.contains("SHARE_") {
            backToID = currentNodeID
        }
        createShareFlow(BackToNodeID: backToID)
        cycle(id: "SHARE_1")
    }
    
    func add(Reply reply: ACReplyOption) {
        let message = JSQMessage(senderId: user.id, displayName: user.name, text: reply.text)
        messages.append(message!)
        delegate?.update()
        self.shareContextReturnID = reply.nextID
        cycle(id: reply.nextID)
    }
    
    func getMessageCount() -> Int {
        return messages.count
    }
    
    func getMessage(Item number: Int) -> JSQMessageData {
        return messages[number]
    }
    
    private func cycle(id: String) {
        self.cycleLock = true
        self.currentNodeID = id
        let node = nodeStore.getNode(forId: id)
        let message: JSQMessageData
        if let image = node.media {
            message = ACMessage(senderId: bot.id,
                                senderDisplayName: bot.name,
                                date: Date(),
                                text: node.text,
                                chart: image)
        } else if let gifPath = node.gifName {
            let gif = UIImage(gifName: gifPath, levelOfIntegrity: 0.8)
            let gifMessage = ACMessage(senderId: bot.id,
                                       senderDisplayName: bot.name,
                                       date: Date(),
                                       text: node.text,
                                       gif: gif)
            message = gifMessage
        } else {
            message = JSQMessage(senderId: bot.id, displayName: bot.name, text: node.text)
        }

        if !node.requiresReply {
            delegate?.animatePendingMessage {
                if self.haltCycle {
                    self.haltCycle = false
                    return
                }
                self.messages.append(message)
                self.delegate?.update()
                self.cycle(id: node.replies[0].nextID)
            }
        } else {
            delegate?.animatePendingMessage {
                if self.haltCycle {
                    self.haltCycle = false
                    return
                }
                self.messages.append(message)
                self.delegate?.update()
                self.delegate?.requestReply(FromList: node.replies)
                self.cycleLock = false
            }
        }
    }
    
    
    private func createShareFlow(BackToNodeID id: String) {
        let shareScript = [ACBasicNode(id: "SHARE_1",
                                       text: "Who would you like me to share this with?",
                                       replies: [ACReplyOption(DisplayText: "Myself",
                                                               ChildID: "SHARE_2"),
                                                 ACReplyOption(DisplayText: "Team",
                                                               ChildID: "SHARE_3"),
                                                ACReplyOption(DisplayText: "Supervisor",
                                                              ChildID: "SHARE_4"),]
                           ),
                           ACBasicNode(id: "SHARE_2",
                                       text: "I’ve sent you an email with details.",
                                       replies: [ACReplyOption(DisplayText: nil, ChildID: "SHARE_5")]
                           ),
                           ACBasicNode(id: "SHARE_3",
                                       text: "I’ve sent an email to your team with details.",
                                       replies: [ACReplyOption(DisplayText: nil, ChildID: "SHARE_5")]
                           ),
                           ACBasicNode(id: "SHARE_4",
                                       text: "I’ve sent an email to your supervisor with details.",
                                       replies: [ACReplyOption(DisplayText: nil, ChildID: "SHARE_5")]
                           ),
                           ACBasicNode(id: "SHARE_5",
                                       text: "Send this to another person?",
                                       replies: [ACReplyOption(DisplayText: "Yes",
                                                               ChildID: "SHARE_1"),
                                                 ACReplyOption(DisplayText: "No, I'm done.",
                                                               ChildID: "SHARE_6")]
                           ),
                           ACBasicNode(id: "SHARE_6",
                                       text: "Okay! Back to our chat...",
                                       replies: [ACReplyOption(DisplayText: nil, ChildID: id)]
                           )]
        nodeStore.load(Nodes: shareScript)
    }

    
}

protocol ACFlowControlDelegate: class {
    func animatePendingMessage(callback: @escaping ()->())
    func requestReply(FromList: [ACReplyOption])
    func update()
}
