//
//  ACBasicNode.swift
//  AskClara
//
//  Created by Donovan King on 4/19/17.
//  Copyright © 2017 Donovan King. All rights reserved.
//

import Foundation
import UIKit

class ACBasicNode: NSObject, ACNodable {
    
    var id: String
    var requiresReply: Bool
    var replies: [ACReplyOption]
    var text: String
    var media: UIImage? = nil
    var gifName: String? = nil
    
    init(id: String, text: String, replies: [ACReplyOption]) {
        self.id = id
        self.replies = replies
        if replies.count == 1 && replies[0].isPointer {
            self.requiresReply = false
        } else {
            self.requiresReply = true
        }
        self.text = text
    }
    
    convenience init(id: String, text: String, replies: [ACReplyOption], image: UIImage) {
        self.init(id: id, text: text, replies: replies)
        self.media = image
    }
    
    convenience init(id: String, text: String, replies: [ACReplyOption], gifNamed gif: String) {
        self.init(id: id, text: text, replies: replies)
        self.gifName = gif
    }
    
}
