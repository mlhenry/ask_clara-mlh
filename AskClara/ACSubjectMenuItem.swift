//
//  ACSubjectMenuItem.swift
//  AskClara
//
//  Created by Donovan King on 4/27/17.
//  Copyright © 2017 Donovan King. All rights reserved.
//

import Foundation


class ACSubjectMenuItem: NSObject {

    let pointerId: String
    let displayText: String
    
    init(pointer: String, text: String) {
        self.pointerId = pointer
        self.displayText = text
        super.init()
    }
    
}
