//
//  PinViewController.swift
//  
//
//  Created by Donovan King on 4/11/17.
//
//

import Foundation
import UIKit

class PinViewController: UIViewController, ACPinViewDelegate, LeanTaaSThreadingMixin {
    
    
    @IBOutlet weak var pinEntryView: PinEntryView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pinEntryView.pinViewDelegate = self
        self.activityIndicator.hidesWhenStopped = true
        
        
        Timer.init(timeInterval: 0.4, repeats: false) { (timer) in
            self.pinEntryView.becomeFirstResponder()
        }.fire()
    }

    func pinComplete(EnteredPin: String) {
        pinEntryView.resignFirstResponder()
        
        UIView.animate(withDuration: 0.6,
                       animations: {
                        self.pinEntryView.alpha = 0
                       },
                       completion: { finished in
                        self.activityIndicator.startAnimating()
                       })
        
        Timer.scheduledTimer(withTimeInterval: 2, repeats: false) { (timer) in
            self.GlobalMainQueue.async {
                self.activityIndicator.stopAnimating()
                self.performSegue(withIdentifier: "SignInSegue", sender: nil)
            }
        }
    }
    
    
}
