//
//  ACReplyView.swift
//  AskClara
//
//  Created by Donovan King on 4/17/17.
//  Copyright © 2017 Donovan King. All rights reserved.
//

import Foundation
import UIKit

class ACReplyView: UIView {
    
    @IBOutlet var view: UIView!
    @IBOutlet weak var stackView: UIStackView!
    
    weak var delegate: ReplyViewDelegate?
    var selectedNode: ACReplyOption?
    
    var visible: Bool
    var overlayMode = false
    
    override var alpha: CGFloat {
        didSet {
            overlayMode = alpha < 0.90
        }
    }
    
    override init(frame: CGRect) {
        self.visible = false
        if (frame.width == 0 && frame.height == 0) {
            super.init(frame: CGRect(x: 0,
                                     y: 0,
                                     width: UIScreen.main.bounds.width,
                                     height: 200))
        } else {
            super.init(frame: frame)
        }
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.visible = false
        super.init(coder: aDecoder)
        setupView()
    }
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        var handleTouch = false
        for view in stackView.arrangedSubviews {
            if view.frame.contains(point) {
                let subPoint = self.convert(point, to: view)
                handleTouch = view.point(inside: subPoint, with: event)
            }
        }
        if handleTouch && overlayMode {
            handleTouch = false
            delegate?.myScrollToBottom()
        }
        return handleTouch
    }
    
    func showWith(ReplyOptions options: [ACReplyOption]) {
        
        frame = CGRect(x: 0,
                       y: UIScreen.main.bounds.height - 45,
                       width: frame.width,
                       height: CGFloat(65 * options.count))
        layoutIfNeeded()
        delegate?.replyViewWillReveal()
        
        let cellFrame = CGRect(x: 0,
                               y: 0,
                               width: frame.width,
                               height: 65)
        
        
        for option in options {
            if option.isRangeReply && option.subReplies != nil {
                let cell = ACMultiReplyCell(frame: cellFrame, OptionNode: option.subReplies!, Parent: self)
                stackView.addArrangedSubview(cell)
            } else {
                let cell = ACReplyCell(frame: cellFrame, OptionNode: option, Parent: self)
                stackView.addArrangedSubview(cell)
            }
        }
        
        let newFrame = CGRect(x: frame.origin.x,
                                 y: frame.origin.y - frame.height,
                                 width: frame.width,
                                 height: frame.height)
        
        UIView.animate(withDuration: 0.1, animations: { self.frame = newFrame })
        { (completed) in
            var stagger = 0.0
            self.stackView.arrangedSubviews.forEach({ (view) in
                guard let cell = view as? Revealable else { return }
                cell.reveal(withDelay: stagger)
                stagger = stagger + 0.1
            })
            self.visible = true
        }
        
    }
    
    func hide() {
        let newFrame = CGRect(x: frame.origin.x,
                              y: frame.origin.y + frame.height,
                              width: frame.width,
                              height: frame.height)
        
        UIView.animate(withDuration: 0.2, animations: { self.frame = newFrame })
        { (complete) in
            self.delegate?.replyViewDidHide()
            if let node = self.selectedNode { self.delegate?.userSelected(Message: node) }
            self.cleanUp()
        }
    }
    
    func selectOption(Node node: ACReplyOption, sender: Revealable) {
        guard let options = stackView.arrangedSubviews as? [Revealable] else { return }
        var stagger = 0.1
        for (option) in options {
            if option !== sender {
                option.hide(withDelay: stagger)
                stagger = stagger + 0.1
            } else {
                selectedNode = node
            }
        }
    }
    
    var dismissCounter = 0
    
    func optionHideCompleted() {
        dismissCounter = dismissCounter + 1
        if stackView.arrangedSubviews.count - dismissCounter <= 1 {
            hide()
        }
    }
    
    private func cleanUp() {
        dismissCounter = 0
        for view in stackView.arrangedSubviews {
            view.removeFromSuperview()
        }
        selectedNode = nil
        self.visible = false
    }
    
    private func setupView() {
        let view = viewFromNibForClass()
        view.frame = bounds
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        addSubview(view)
    }
    
    private func viewFromNibForClass() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
    
}
