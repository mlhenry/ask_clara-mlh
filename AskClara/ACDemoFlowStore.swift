//
//  ACDemoFlowStore.swift
//  AskClara
//
//  Created by Donovan King on 4/19/17.
//  Copyright © 2017 Donovan King. All rights reserved.
//

import Foundation
import UIKit


class ACDemoFlowStore: NSObject {
    
    private var nodeStore: Dictionary<String, ACNodable>
    //TODO: Build out error handling node.
    private let errorNode = ACBasicNode(id: "error", text: "There was an ID Error", replies: [])
    
    
    override init() {
        self.nodeStore = Dictionary<String, ACNodable>()
    }
    
    func load(Node node: ACNodable) {
        nodeStore[node.id] = node
    }
    
    func load(Nodes nodes: [ACNodable]) {
        for node in nodes {
            self.load(Node: node)
        }
    }
    
    //      TODO: Figure out what you wanna do for error nodes.
    //      Maybe only use them durring testing?
    func getNode(forId id: String) -> ACNodable {
        if let node = nodeStore[id] {
            return node
        }
        return errorNode
    }
    
}

struct Bot {
    let id = "ask_clara"
    let name = "Clara"
}

struct User {
    let id = "demo_user"
    let name = "Jay"
}

protocol ACNodable: class {
    
    var id: String { get }
    var requiresReply: Bool { get }
    var replies: [ACReplyOption] { get }
    var text: String { get }
    var media: UIImage? { get }
    var gifName: String? { get }
}
