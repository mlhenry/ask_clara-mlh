//
//  ACReplyOption.swift
//  AskClara
//
//  Created by Donovan King on 4/18/17.
//  Copyright © 2017 Donovan King. All rights reserved.
//

import Foundation


class ACReplyOption: NSObject {
    
    let text: String
    let isPointer: Bool
    let isRangeReply: Bool
    let nextID: String
    var subReplies: [ACReplyOption]?
    
    init(DisplayText text: String?, ChildID id: String) {
        if let replyText = text {
            self.text = replyText
            self.isPointer = false
        } else {
            self.text = ""
            self.isPointer = true
        }
        self.nextID = id
        self.isRangeReply = false
        super.init()
    }
    
    init(ChildID id: String) {
        self.text = ""
        self.isPointer = true
        self.nextID = id
        self.isRangeReply = false
    }
    
    init(SubReplies: [ACReplyOption]) {
        self.isRangeReply = true
        self.subReplies = SubReplies
        self.text = ""
        self.nextID = ""
        self.isPointer = false
        super.init()
    }
    
    
    
}
