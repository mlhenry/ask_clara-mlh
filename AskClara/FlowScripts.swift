//
//  FlowScripts.swift
//  AskClara
//
//  Created by Donovan King on 4/21/17.
//  Copyright © 2017 Donovan King. All rights reserved.
//

import Foundation


class DemoFlow {

        static let menuItems = [ACSubjectMenuItem(pointer: "4", text: "portfolio"),
                                ACSubjectMenuItem(pointer: "31", text: "with special issues"),
                                ACSubjectMenuItem(pointer: "57", text: "that just came in"),
                                ACSubjectMenuItem(pointer: "NO-GO", text: "that had significant recent changes"),
                                ACSubjectMenuItem(pointer: "NO-GO", text: "where estimates were off")]

    static let nodes = [ACBasicNode(id: "1",
                                        text: "Welcome back, Jayant!",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "2")]
            ),
                            ACBasicNode(id: "2",
                                        text: "Last time you were here you started with a look at your claims portfolio.",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "3")]
            ),
//                            StaticNodes.binary,
                            ACBasicNode(id: "3",
                                        text: "Want to start there again?",
                                        replies: [ACReplyOption(DisplayText: "yes", ChildID: "4"),
                                                 ACReplyOption(DisplayText: "no, see something else", ChildID: "NO-GO")]
            ),
                            ACBasicNode(id: "4",
                                        text: "Great, let's have a look at your claims portfolio...",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "5")]
            ),
                            ACBasicNode(id: "5",
                                        text: "Which profile would you like to see?",
                                        replies: [ACReplyOption(DisplayText: "complexity/cost buckets", ChildID: "6"),
                                                  ACReplyOption(DisplayText: "attorney involvement", ChildID: "NO-GO")]
            ),
                            ACBasicNode(id: "6",
                                        text: "Here's the split by complexity:",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "8")],
                                        image: #imageLiteral(resourceName: "donut-complexity")
            ),
                            ACBasicNode(id: "7",
                                        text: "Here's the split by attorney involvement:",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "8")],
                                        image: #imageLiteral(resourceName: "donut-attorney")
            ),
                            ACBasicNode(id: "8",
                                        text: "Would you like to drill down into a complexity level?",
                                        replies:[ACReplyOption(SubReplies: [ACReplyOption(DisplayText: "level 1", ChildID: "NO-GO"),
                                                                    ACReplyOption(DisplayText: "level 2", ChildID: "NO-GO"),
                                                                    ACReplyOption(DisplayText: "level 3", ChildID: "NO-GO"),
                                                                    ACReplyOption(DisplayText: "level 4", ChildID: "10")]),
                                                                    ACReplyOption(DisplayText: "no, see something else", ChildID: "NO-GO")]
            ),
                            ACBasicNode(id: "10",
                                        text: "You got it, let's take a deeper look...",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "11")]
            ),
                            ACBasicNode(id: "11",
                                        text: "You have 2 claims with level 4 complexity...",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "12")]
            ),
                            ACBasicNode(id: "12",
                                        text: "1 of 2 claims",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "13")],
                                        image: #imageLiteral(resourceName: "list-claim-complex-1of2")
            ),
                            ACBasicNode(id: "13",
                                        text: "2 of 2 claims",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "14")],
                                        image: #imageLiteral(resourceName: "list-claim-complex-2of2")
            ),
                            ACBasicNode(id: "14",
                                        text: "Would you like to dig into a claim?",
                                        replies: [ACReplyOption(SubReplies: [ACReplyOption(DisplayText: "B456", ChildID: "NO-GO"),
                                                                             ACReplyOption(DisplayText: "A123", ChildID: "17")]),
                                                  ACReplyOption(DisplayText: "no, see something else", ChildID: "NO-GO")]
            ),
                            ACBasicNode(id: "17",
                                        text: "Here is a summary of claim A123:",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "18")]
            ),
                            ACBasicNode(id: "18",
                                        text: "1 of 2 claims",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "19")],
                                        image: #imageLiteral(resourceName: "summary-claims-complex-A123")
            ),
                            ACBasicNode(id: "19",
                                        text: "Want to see the full details of this claim?",
                                        replies: [ACReplyOption(DisplayText: "yes", ChildID: "NO-GO"),
                                                  ACReplyOption(DisplayText: "see next claim", ChildID: "20"),
                                                  ACReplyOption(DisplayText: "no, see something else", ChildID: "NO-GO")]
            ),
                            ACBasicNode(id: "20",
                                        text: "Here is a summary of claim B456:",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "21")]
            ),
                            ACBasicNode(id: "21",
                                        text: "2 of 2 claims",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "22")],
                                        image: #imageLiteral(resourceName: "summary-claims-complex-B456")
            ),
                            ACBasicNode(id: "22",
                                        text: "Want to see the full details of this claim?",
                                        replies: [ACReplyOption(DisplayText: "yes", ChildID: "NO-GO"),
                                                  ACReplyOption(DisplayText: "no, see something else", ChildID: "23")]
            ),
                            ACBasicNode(id: "23",
                                        text: "You've seen all of your level 4 claims...",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "24")]
            ),
                            ACBasicNode(id: "24",
                                        text: "Would you like to dig into another complexity level?",
                                        replies:[ACReplyOption(SubReplies: [ACReplyOption(DisplayText: "level 1", ChildID: "NO-GO"),
                                                                            ACReplyOption(DisplayText: "level 2", ChildID: "NO-GO"),
                                                                            ACReplyOption(DisplayText: "level 3", ChildID: "NO-GO"),
                                                                            ACReplyOption(DisplayText: "level 4", ChildID: "NO-GO")]),
                                                 ACReplyOption(DisplayText: "no, see something else", ChildID: "25")]
            ),
                            ACBasicNode(id: "25",
                                        text: "Sure thing...",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "26")]
            ),
                            ACBasicNode(id: "26",
                                        text: "Looks like the number of claims with special issues has increased...",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "30")]
            ),
                            //padded node numbers +3
                            ACBasicNode(id: "30",
                                        text: "Talk about that next?",
                                        replies: [ACReplyOption(DisplayText: "yes", ChildID: "31"),
                                                  ACReplyOption(DisplayText: "no, see something else", ChildID: "NO-GO")]
            ),
                            ACBasicNode(id: "31",
                                        text: "Great, let's see what's going on for your claims with special issues...",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "32")]
            ),
                            ACBasicNode(id: "32",
                                        text: "What type of special issue would you like to know more about?",
                                        replies: [ACReplyOption(DisplayText: "comorbidities", ChildID: "NO-GO"),
                                                  ACReplyOption(DisplayText: "opioids", ChildID: "33")]
            ),
                            ACBasicNode(id: "33",
                                        text: "Sounds good...",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "34")]
            ),
                            ACBasicNode(id: "34",
                                        text: "Here is a look at your claims with opioid issues:",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "35")]
            ),
                            ACBasicNode(id: "35",
                                        text: "Claims with Opioid Issues",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "36")],
                                        image: #imageLiteral(resourceName: "donut-opioid")
            ),
                            ACBasicNode(id: "36",
                                        text: "Yikes!...",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "37")]
            ),
                            ACBasicNode(id: "37",
                                        text: "12 claims have an opioid issue...",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "27")]
            ),
                            ACBasicNode(id: "27",
                                        text: "See your claims with opioid issues?",
                                        replies: [ACReplyOption(DisplayText: "yes", ChildID: "38"),
                                                  ACReplyOption(DisplayText: "no, see something else", ChildID: "NO-GO")]
            ),
                            ACBasicNode(id: "38",
                                        text: "Here are your claims that have been flagged for opioid issues:",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "39")]
            ),
                            ACBasicNode(id: "39",
                                        text: "1 of 12",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "40")],
                                        image: #imageLiteral(resourceName: "list-claim-opioid-K123")
            ),
                            ACBasicNode(id: "40",
                                        text: "2 of 12",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "41")],
                                        image: #imageLiteral(resourceName: "list-claim-opioid-L456")
            ),
                            ACBasicNode(id: "41",
                                        text: "3 of 12",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "42")],
                                        image: #imageLiteral(resourceName: "list-claim-complex-1of2")
            ),
                            ACBasicNode(id: "42",
                                        text: "Would you like to...",
                                        replies: [ACReplyOption(DisplayText: "dig into a claim", ChildID: "43"),
                                                  ACReplyOption(DisplayText: "see more claims", ChildID: "NO-GO"),
                                                  ACReplyOption(DisplayText: "see something else", ChildID: "NO-GO")]
            ),
                            ACBasicNode(id: "43",
                                        text: "Dig into claim...",
                                        replies: [ACReplyOption(SubReplies: [ACReplyOption(DisplayText: "A123", ChildID: "NO-GO"),
                                                                             ACReplyOption(DisplayText: "L456", ChildID: "NO-GO"),
                                                                             ACReplyOption(DisplayText: "K123", ChildID: "44")]),
                                                  ACReplyOption(DisplayText: "no, see something else", ChildID: "NO-GO")]
            ),
                            ACBasicNode(id: "44",
                                        text: "Here is a summary of claim K123:",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "45")]
            ),
                            ACBasicNode(id: "45",
                                        text: "3 of 12",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "46")],
                                        image: #imageLiteral(resourceName: "summary-claim-opioid-K123")
            ),
                            ACBasicNode(id: "46",
                                        text: "Want to see the full details of this claim?",
                                        replies: [ACReplyOption(DisplayText: "yes", ChildID: "NO-GO"),
                                                  ACReplyOption(DisplayText: "no, see something else", ChildID: "47")]
            ),
                            ACBasicNode(id: "47",
                                        text: "You have some new claims with comorbidities...",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "48")]
            ),
                            ACBasicNode(id: "48",
                                        text: "See those next?",
                                        replies: [ACReplyOption(DisplayText: "yes", ChildID: "NO-GO"),
                                                  ACReplyOption(DisplayText: "no, see something else", ChildID: "55"),
                                                  ACReplyOption(DisplayText: "say goodbye", ChildID: "NO-GO")]
            ),
                            //padded node numbers +7
                            ACBasicNode(id: "55",
                                        text: "Got it. Just a reminder...",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "56")]
            ),
                            ACBasicNode(id: "56",
                                        text: "You can tap the button below anytime to change the conversation.",
                                        replies: []
            ),
                            //change conversation + check menu node numbers
                            ACBasicNode(id: "57",
                                        text: "Sure thing...",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "58")]
            ),
                            ACBasicNode(id: "58",
                                        text: "Let's take a look at your claims that just came in...",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "59")]
            ),
                            ACBasicNode(id: "59",
                                        text: "You have 3 new claims from the past 72 hours.",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "60")]
            ),
                            ACBasicNode(id: "60",
                                        text: "See your new claims?",
                                        replies: [ACReplyOption(DisplayText: "yes", ChildID: "49"),
                                                  ACReplyOption(DisplayText: "see something else", ChildID: "NO-GO")]
            ),
                            ACBasicNode(id: "49",
                                        text: "Here are your 3 new claims:",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "61")]
            ),
                            ACBasicNode(id: "61",
                                        text: "1 of 3 claims",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "62")],
                                        image: #imageLiteral(resourceName: "list-claim-new-P123")
            ),
                            ACBasicNode(id: "62",
                                        text: "2 of 3 claims",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "63")],
                                        image: #imageLiteral(resourceName: "list-claim-new-Q456")
            ),
                            ACBasicNode(id: "63",
                                        text: "3 of 3 claims",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "64")],
                                        image: #imageLiteral(resourceName: "list-claim-new-R789")
            ),
                            ACBasicNode(id: "64",
                                        text: "Would you like to...",
                                        replies: [ACReplyOption(DisplayText: "dig into a claim", ChildID: "65"),
                                                  ACReplyOption(DisplayText: "no, see something else", ChildID: "NO-GO")]
            ),
                            ACBasicNode(id: "65",
                                        text: "Dig into claim...",
                                        replies: [ACReplyOption(SubReplies: [ACReplyOption(DisplayText: "R789", ChildID: "NO-GO"),
                                                                             ACReplyOption(DisplayText: "Q456", ChildID: "NO-GO"),
                                                                             ACReplyOption(DisplayText: "P123", ChildID: "66")]),
                                                  ACReplyOption(DisplayText: "no, see something else", ChildID: "NO-GO")]
            ),
                            ACBasicNode(id: "66",
                                        text: "Here is a summary of claim P123:",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "67")]
            ),
                            ACBasicNode(id: "67",
                                        text: "1 of 3",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "68")],
                                        image: #imageLiteral(resourceName: "summary-claim-new-P123")
            ),
                            ACBasicNode(id: "68",
                                        text: "Where to next?",
                                        replies: [ACReplyOption(DisplayText: "details of claim", ChildID: "NO-GO"),
                                                  ACReplyOption(DisplayText: "next claim", ChildID: "69"),
                                                  ACReplyOption(DisplayText: "no, see something else", ChildID: "NO-GO")]
            ),
                            ACBasicNode(id: "69",
                                        text: "Here is a summary of claim Q456:",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "70")]
            ),
                            ACBasicNode(id: "70",
                                        text: "2 of 3",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "71")],
                                        image: #imageLiteral(resourceName: "summary-claim-new-Q456")
            ),
                            ACBasicNode(id: "71",
                                        text: "Would you like to see the last of your new claims?",
                                        replies: [ACReplyOption(DisplayText: "yes", ChildID: "72"),
                                                  ACReplyOption(DisplayText: "no, see something else", ChildID: "NO-GO")]
            ),
                            ACBasicNode(id: "72",
                                        text: "3 of 3",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "73")],
                                        image: #imageLiteral(resourceName: "summary-claim-new-R789")
            ),
                            ACBasicNode(id: "73",
                                        text: "You've seen all of your new claims.",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "80")]
            ),
                            //padded node numbers +6
                            ACBasicNode(id: "80",
                                        text: "I've taken a look at your estimates, looks like a few are off...",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "81")]
            ),
                            ACBasicNode(id: "81",
                                        text: "Would you like to see those next?",
                                        replies: [ACReplyOption(DisplayText: "yes", ChildID: "NO-GO"),
                                                  ACReplyOption(DisplayText: "no, see something else", ChildID: "82")]
            ),
                            ACBasicNode(id: "82",
                                        text: "Got it...",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "83")]
            ),
                            ACBasicNode(id: "83",
                                        text: "How about claims that have recently had big changes?",
                                        replies: [ACReplyOption(DisplayText: "yes", ChildID: "NO-GO"),
                                                  ACReplyOption(DisplayText: "no, see something else", ChildID: "NO-GO"),
                                                  ACReplyOption(DisplayText: "say goodbye", ChildID: "84")]
            ),
                            ACBasicNode(id: "84",
                                        text: "Really enjoyed our conversation...",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "85")]
            ),
                            ACBasicNode(id: "85",
                                        text: "Wow, it's been a busy day...",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "86")]
            ),
                            ACBasicNode(id: "86",
                                        text: "Feeling like this?",
                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "87")],
                                        gifNamed: "sleep-ice-cream"
            ),
                            ACBasicNode(id: "87",
                                        text: "I have a sense of humor too 😉",
                                        replies: [ACReplyOption(DisplayText:nil, ChildID: "88")]
            ),
                            ACBasicNode(id: "88",
                                        text: "Nice to chat, see you next time!",
                                        replies: []
            ),
                            
                            
]
}
//GOODBYE FLOW///

//                            ACBasicNode(id: "42",
//                                        text: "Really enjoyed our conversation today...",
//                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "43")]
//            ),
//                            ACBasicNode(id: "43",
//                                        text: "Mind answering two quick questions?",
//                                        replies: [ACReplyOption(DisplayText: "sure", ChildID: "44"),
//                                                  ACReplyOption(DisplayText: "maybe next time", ChildID: "")]
//            ),
//                            ACBasicNode(id: "44",
//                                        text: "Thanks, I know it's been a busy day...",
//                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "45")]
//            ),
//                            ACBasicNode(id: "45",
//                                        text: "Did I guide you to the information you needed?",
//                                        replies: [ACReplyOption(DisplayText: "right on the money", ChildID: ""),
//                                                  ACReplyOption(DisplayText: "pretty much", ChildID: "46"),
//                                                  ACReplyOption(DisplayText: "could be better", ChildID: "")]
//            ),
//                            ACBasicNode(id: "46",
//                                        text: "Got it...",
//                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "47")]
//            ),
//
//                            ACBasicNode(id: "47",
//                                        text: "Help me get to know you a bit better...",
//                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "48")]
//            ),
//                            ACBasicNode(id: "48",
//                                        text: "Would you like me to be able to...",
//                                        replies: [ACReplyOption(DisplayText: "answer more questions", ChildID: ""),
//                                                  ACReplyOption(DisplayText: "talk a little faster", ChildID: "49"),
//                                                  ACReplyOption(DisplayText: "give fewer options", ChildID: "")]
//            ),
//                            ACBasicNode(id: "49",
//                                        text: "You got it..",
//                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "51")]
//            ),
//                            ACBasicNode(id: "51",
//                                        text: "I'll talk a little faster next time...",
//                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "52")]
//            ),
//                            ACBasicNode(id: "52",
//                                        text: "Thanks for your feedback! ---Insert GIF here---",
//                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "53")],
//                                        image: #imageLiteral(resourceName: "opioid-donut")
//            ),
//                            ACBasicNode(id: "53",
//                                        text: "I have a sense of humor too 😉",
//                                        replies: [ACReplyOption(DisplayText:nil, ChildID: "54")]
//            ),
//                            ACBasicNode(id: "54",
//                                        text: "Nice to chat, see you next time!",
//                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "55")]
//            ),
//                            ACBasicNode(id: "55",
//                                        text: "Current Demo Finished",
//                                        replies: [ACReplyOption(DisplayText: nil, ChildID: "")]

//    let nodes = [ACBasicNode(id: "1",
//                             text: "hello",
//                             replies: [ACReplyOption(DisplayText: nil, ChildID: "2")]
//        ),
//                 ACBasicNode(id: "2",
//                             text: "How are you?",
//                             replies: [ACReplyOption(DisplayText: "Good", ChildID: "3"),
//                                       ACReplyOption(DisplayText: "Meh", ChildID: "4")]
//        ),
//                 ACBasicNode(id: "3",
//                             text: "Awesome! Wanna see a cool trick?",
//                             replies: [ACReplyOption(DisplayText: "Entertain me", ChildID: "6"),
//                                       ACReplyOption(DisplayText: "Nah", ChildID: "5")]
//        ),
//                 ACBasicNode(id: "4",
//                             text: "Uh oh, well wanna see a cool trick?",
//                             replies: [ACReplyOption(DisplayText: "Sure", ChildID: "6"),
//                                       ACReplyOption(DisplayText: "No, I'm too sad", ChildID: "5")]
//        ),
//                 ACBasicNode(id: "5",
//                             text: "Hrm...I don't care. I'm showing you anyway!",
//                             replies: [ACReplyOption(DisplayText: nil, ChildID: "6")]
//        ),
//                 ACBasicNode(id: "6",
//                             text: "I'm a bot who can beat box!",
//                             replies: [ACReplyOption(DisplayText: nil, ChildID: "7")]
//        ),
//                 ACBasicNode(id: "7",
//                             text: "👢 & 🐈s, 👢 & 🐈s, 👢 & 🐈s",
//                             replies: [ACReplyOption(DisplayText: nil, ChildID: "8")]
//        ),
//                 ACBasicNode(id: "8",
//                             text: "Did you like that?",
//                             replies: [ACReplyOption(DisplayText: "You're Chill", ChildID: "9"),
//                                       ACReplyOption(DisplayText: "Try another hobby", ChildID: "10")]
//        ),
//                 ACBasicNode(id: "9",
//                             text: "Thank you ❤️",
//                             replies: [ACReplyOption(DisplayText: nil, ChildID: "11")]
//        ),
//                 ACBasicNode(id: "10",
//                             text: "😐 I'm a bot. I don't have hobbies.",
//                             replies: [ACReplyOption(DisplayText: nil, ChildID: "11")]
//        ),
//                 ACBasicNode(id: "11",
//                             text: "Did you know I have a sister?",
//                             replies: [ACReplyOption(DisplayText: "Yes", ChildID: "12"),
//                                       ACReplyOption(DisplayText: "No", ChildID: "12")]
//        ),
//                 ACBasicNode(id: "12",
//                             text: "Well her name is SWiM and I think she's pretty cool. <3",
//                             replies: [ACReplyOption(DisplayText: nil, ChildID: "13")]
//        ),
//                 ACBasicNode(id: "13",
//                             text: "She works with people who are having a rough time",
//                             replies: []
//        )
//    ]
// Define static nodes here for standard responses
// Need to manage ChildIDs
//class StaticNodes {
//    
//    static let binary = ACBasicNode(id: "binary",
//                                    text: "",
//                                    replies: [ACReplyOption(DisplayText: "yes", ChildID: "yes"),
//                                              ACReplyOption(DisplayText: "no, see something else", ChildID: "no")]
//    )
//    static let level = ACBasicNode(id: "level#",
//                                    text: "",
//                                    replies: [ACReplyOption(SubReplies: [ACReplyOption(DisplayText: "level 1", ChildID: "L1"),
//                                                                         ACReplyOption(DisplayText: "level 2", ChildID: "L2"),
//                                                                         ACReplyOption(DisplayText: "level 3", ChildID: "L3"),
//                                                                         ACReplyOption(DisplayText: "level 4", ChildID: "L4")]),
//                                              ACReplyOption(DisplayText: "no, see something else", ChildID: "no-level#")]
//    )
//
//    static let claimnum = ACBasicNode(id: "claim#",
//                                    text: "",
//                                    replies: [ACReplyOption(SubReplies: [ACReplyOption(DisplayText: "123", ChildID: "123"),
//                                                                         ACReplyOption(DisplayText: "456", ChildID: "456"),
//                                                                         ACReplyOption(DisplayText: "789", ChildID: "789")]),
//                                                  ACReplyOption(DisplayText: "no, see something else", ChildID: "no-claim#")]
//    )
//    static let claimsdrilldonut = ACBasicNode(id: "claimsdrilldonut",
//                                                text: "",
//                                                replies: [ACReplyOption(DisplayText: "yes", ChildID: "yes"),
//                                                          ACReplyOption(DisplayText: "see more claims", ChildID: "see more claims"),
//                                                          ACReplyOption(DisplayText: "no, see something else", ChildID: "no")]
//    )
//    static let claimsdrillsum = ACBasicNode(id: "claimsdrillsum",
//                                         text: "",
//                                         replies: [ACReplyOption(DisplayText: "details", ChildID: "details"),
//                                                   ACReplyOption(DisplayText: "no, see something else", ChildID: "no")]
//    )
//    static let claimsdrilldetails = ACBasicNode(id: "claimsdrilldetails",
//                                         text: "",
//                                         replies: [ACReplyOption(DisplayText: "see more claims", ChildID: "see more claims"),
//                                                   ACReplyOption(DisplayText: "see another level", ChildID: "see another level"),
//                                                   ACReplyOption(DisplayText: "no, see something else", ChildID: "no")]
//    )
//
//    
//}
//
//
