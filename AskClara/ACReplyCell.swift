//
//  ACReplyCell.swift
//  AskClara
//
//  Created by Donovan King on 4/17/17.
//  Copyright © 2017 Donovan King. All rights reserved.
//

import Foundation
import UIKit

class ACReplyCell: UIView, Revealable {
    
    
    @IBOutlet weak var optionButton: UIButton!
    @IBOutlet weak var optionButtonTrailing: NSLayoutConstraint!
    @IBOutlet weak var buttonWidthContraint: NSLayoutConstraint!
    
    var node: ACReplyOption
    weak var replyView: ACReplyView?
    
    
    required init?(coder aDecoder: NSCoder) {
        return nil
    }
    
    init(frame: CGRect, OptionNode node: ACReplyOption, Parent parent: ACReplyView) {
        self.node = node
        self.replyView = parent
        super.init(frame: frame)
        self.setupView()
    }
    
    @IBAction func optionSelected(_ sender: Any) {
        if node.nextID == "NO-GO" { return }
        replyView?.selectOption(Node: node, sender: self)
    }
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        if optionButton.frame.contains(point) {
            return true
        }
        return false
    }
    
    func reveal(withDelay delay: Double = 0) {
        optionButtonTrailing.constant = 10
        UIView.animate(withDuration: 0.4,
                       delay: delay,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 4,
                       options: .curveEaseIn,
                       animations: { self.layoutIfNeeded() })
    }
    
    func hide(withDelay delay: Double = 0) {
        optionButtonTrailing.constant = -buttonWidthContraint.constant
        UIView.animate(withDuration: 0.4,
                       delay: delay,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 4,
                       options: .curveEaseIn,
                       animations: { self.layoutIfNeeded() })
        { (comp) in
            self.replyView?.optionHideCompleted()
        }
    }
    
    private func setupView() {
        let view = viewFromNibForClass()
        view.frame = bounds
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        addSubview(view)
        self.setupButton()
    }
    
    private func setupButton() {
        optionButton.setTitle(node.text, for: .normal)
        layoutIfNeeded()
        self.optionButton.layer.cornerRadius = optionButton.bounds.height/4
        if let labelWidth = optionButton.titleLabel?.frame.width {
            buttonWidthContraint.constant = labelWidth + 20
        }
        optionButtonTrailing.constant = -buttonWidthContraint.constant
    }
    
    private func viewFromNibForClass() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }

}

protocol Revealable: class {
    func reveal(withDelay delay: Double)
    func hide(withDelay delay: Double)
}
