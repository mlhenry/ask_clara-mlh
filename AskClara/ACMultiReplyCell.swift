//
//  ACMultiReplyCell.swift
//  AskClara
//
//  Created by Donovan King on 4/26/17.
//  Copyright © 2017 Donovan King. All rights reserved.
//

import Foundation
import UIKit

class ACMultiReplyCell: UIView, Revealable {
    
    var replies: [ACReplyOption]
    weak var replyView: ACReplyView?
    
    var buttons = [(xPosConstraint: NSLayoutConstraint, button: UIButton, reply: ACReplyOption)]()
    
    init(frame: CGRect, OptionNode replies: [ACReplyOption], Parent parent: ACReplyView) {
        self.replies = replies
        self.replyView = parent
        super.init(frame: frame)
        self.setupView(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        return nil
    }
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        var handleTouch = false
        for buttonTuple in buttons {
            if buttonTuple.button.frame.contains(point) {
                handleTouch = true
            }
        }
        return handleTouch
    }
    
    func reveal(withDelay delay: Double = 0) {
        var xPos: CGFloat = 10
        for (buttonInfo) in buttons {
            buttonInfo.xPosConstraint.constant = -xPos
            xPos = xPos + buttonInfo.button.bounds.width + 10
        }
        
        UIView.animate(withDuration: 0.4,
                       delay: delay,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 4,
                       options: .curveEaseIn,
                       animations: { self.layoutIfNeeded() })
    }
    
    func hide(withDelay delay: Double) {
        
        for (buttonInfo) in buttons {
            buttonInfo.xPosConstraint.constant = buttonInfo.button.bounds.width
        }
        
        UIView.animate(withDuration: 0.4,
                       delay: delay,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 4,
                       options: .curveEaseIn,
                       animations: { self.layoutIfNeeded() })
        { (comp) in
            self.replyView?.optionHideCompleted()
        }
    }
    
    private func setupView(frame: CGRect) {
        for reply in replies {
            let attr = [ NSFontAttributeName : UIFont(name: "SFUIText-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 15),
                         NSForegroundColorAttributeName : UIColor.white]
            let attrString = NSAttributedString(string: reply.text, attributes: attr)
            let button = UIButton(type: .system)
            button.translatesAutoresizingMaskIntoConstraints = false
            button.backgroundColor = UIColor(red:0.00, green:0.74, blue:0.49, alpha:1.0)
            button.setAttributedTitle(attrString, for: .normal)
            button.addConstraint(NSLayoutConstraint(item: button,
                                                    attribute: .height,
                                                    relatedBy: .equal,
                                                    toItem: nil,
                                                    attribute: .notAnAttribute,
                                                    multiplier: 1,
                                                    constant: frame.height * 0.6))
            let width = attrString.size().width + 20 >= 40 ? attrString.size().width + 20 : 40
            button.addConstraint(NSLayoutConstraint(item: button,
                                                    attribute: .width,
                                                    relatedBy: .equal,
                                                    toItem: nil,
                                                    attribute: .notAnAttribute,
                                                    multiplier: 1,
                                                    constant: width))
            button.frame = CGRect(x: 0, y: 0, width: width, height: frame.height * 0.6)
            button.layer.cornerRadius = button.frame.height/4
            if reply.nextID != "NO-GO" {
                button.addTarget(self,
                                 action: #selector(ACMultiReplyCell.replySelected(sender:)),
                                 for: .touchUpInside)
            }
            self.addSubview(button)
            let centerY = NSLayoutConstraint(item: button,
                                             attribute: .centerY,
                                             relatedBy: .equal,
                                             toItem: self,
                                             attribute: .centerY,
                                             multiplier: 1,
                                             constant: 0)
            let xPos = NSLayoutConstraint(item: button,
                                          attribute: .trailing,
                                          relatedBy: .equal,
                                          toItem: self,
                                          attribute: .trailing,
                                          multiplier: 1,
                                          constant: button.frame.width)
            let tupe = (xPosConstraint: xPos, button: button, reply)
            buttons.append(tupe)
            self.addConstraints([centerY, xPos])
        }
    }
    
    func replySelected(sender: UIButton) {
        var reply: ACReplyOption?
        for (buttonInfo) in buttons {
            if buttonInfo.button !== sender {
                buttonInfo.xPosConstraint.constant = buttonInfo.button.bounds.width
            } else {
                reply = buttonInfo.reply
                buttonInfo.xPosConstraint.constant = -10
            }
        }
        UIView.animate(withDuration: 0.4,
                       delay: 0,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 4,
                       options: .curveEaseIn,
                       animations: { self.layoutIfNeeded() })
        { (comp) in
            if let unwrappedReply = reply {
                self.replyView?.selectOption(Node: unwrappedReply, sender: self)
            }
        }
    }
    
}
